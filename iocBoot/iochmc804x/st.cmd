#!../../bin/linux-x86_64/hmc804x

#- You may have to change hmc804x to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/hmc804x.dbd"
hmc804x_registerRecordDeviceDriver pdbbase

epicsEnvSet("STREAM_PROTOCOL_PATH", "${TOP}/db")
epicsEnvSet("ASYN_PORT", "HMC804x-asyn-port")
drvAsynIPPortConfigure("${ASYN_PORT}", "${DEVICE_IP}:5025")

## Load record instances
dbLoadRecords("db/${MODEL=hmc8043}.db", "PORT=${ASYN_PORT},P=${P},R=${R=}")

cd "${TOP}/iocBoot/${IOC}"
iocInit
